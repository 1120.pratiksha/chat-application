'use strict';

const bcrypt=require('bcrypt')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('Users',[
     {
     firstName:'Pratiksha',
     lastName:'Deshmukh',
     email:'pratiksha@gmail.com',
     password:bcrypt.hashSync('Pratiksha',10),
     gender:'Female'
   },
   {
    firstName:'Kailash',
    lastName:'Chakka',
    email:'kailash@gmail.com',
    password:'Kailash',
    gender:'Male'
  },
  {
    firstName:'Niranjan',
    lastName:'Reddy',
    email:'niranjan@gmail.com',
    password:'Niranjan',
    gender:'Male'
  }
  ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users',null,{})
  }
};
