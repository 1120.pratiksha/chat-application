import {FETCH_CHATS} from '../actions/chat'

const initialState = {
    chats: [],
    currentChat: {},
    socket: {},
    newMessage: { chatId: null, seen: null },
    scrollBottom: 0,
    senderTyping: { typing: false }
}